import React from 'react';
// @ts-ignore
import { Button } from 'ui-library';

const App = () => {
    return (
        <div>
            <h1>Hello World!</h1>
            <Button>I am a working UI library button!</Button>
        </div>
    );
}

export default App;
